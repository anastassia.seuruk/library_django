from django.shortcuts import render, redirect
from django.views import View

from books.forms import BookCreationForm
from books.models import Book
from users.forms import UserCreationForm
from users.models import User


class UserIndexView(View):
    form_class = UserCreationForm
    template_name = 'users/index.html'

    def get(self, request, *args, **kwargs):
        users = User.objects.all()
        form = self.form_class()
        return render(
            request,
            self.template_name,
            {'form': form, 'users': users}
        )

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return redirect('users:index')

        return render(request, self.template_name, {'form': form})


class UserBooksView(View):
    form_class = BookCreationForm
    template_name = 'users/books.html'

    def get(self, request, *args, **kwargs):
        user_id = kwargs.get('user_id')
        books = Book.objects.filter(user__id=user_id)
        form = self.form_class()
        return render(
            request,
            self.template_name,
            {'form': form, 'books': books}
        )

    def post(self, request, *args, **kwargs):
        user_id = kwargs.get('user_id')
        user = User.objects.get(id=user_id)
        form = self.form_class(request.POST)
        if form.is_valid():
            book = form.save(commit=False)
            book.user = user
            book.save()
            user.books.add(book.id)
            user.save()
            return redirect('users:index')

        return render(request, self.template_name, {'form': form})
