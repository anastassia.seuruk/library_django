from django.urls import path

from users import views

app_name = 'users'
urlpatterns = [
    path('', views.UserIndexView.as_view(), name='index'),
    path('user/<int:user_id>/books/', views.UserBooksView.as_view(),
         name='books'),
]
