from datetime import date

from django.test import TestCase, Client
from django.urls import reverse

from books.models import Book
from users.models import User


class UserTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user1 = User.objects.create(
            last_name="Lion", first_name="Anna"
        )
        self.user2 = User.objects.create(
            last_name="Mors", first_name="Anabel",
        )
        self.book1 = Book.objects.create(
            title="American Tragedy",
            author="Theodore Draiser",
            publication_date=date(1925, 5, 16)
        )
        self.book2 = Book.objects.create(
            title="Titan",
            author="Theodore Draiser",
            publication_date=date(1913, 8, 16)
        )
        self.user1.books.add(self.book1)
        self.user2.books.add(self.book1)
        self.user2.books.add(self.book2)

    def test_users_index_view(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.content)

        response = self.client.post(
            '/', {'first_name': 'john', 'last_name': 'smith'}
        )
        self.assertEqual(response.status_code, 200)

    def test_users_books_view(self):
        url = reverse('users:books', kwargs={'user_id': self.user2.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(
            url,
            {'title': 'Atlas Struggled',
             'author': 'Ayn Rand',
             'publication_date': '2019-08-21'}
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.user2.books.count(), 3)
