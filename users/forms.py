from django.forms import ModelForm
from users.models import User


class UserCreationForm(ModelForm):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'date_of_birth')
