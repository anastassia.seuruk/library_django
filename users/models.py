from django.db import models

from books.models import Book


class User(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=40)
    email = models.EmailField(null=True, blank=True)
    date_of_birth = models.DateField(null=True)
    books = models.ManyToManyField(Book, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
