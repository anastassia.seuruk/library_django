from django import forms
from books.models import Book


class BookCreationForm(forms.ModelForm):

    class Meta:
        model = Book
        fields = ('title', 'author', 'publication_date')


class BookUpdateForm(forms.ModelForm):
    title = forms.CharField(required=False)
    author = forms.CharField(required=False)
    publication_date = forms.DateField(required=False)

    class Meta:
        model = Book
        fields = ('title', 'author', 'publication_date')
