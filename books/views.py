from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView

from books.forms import BookUpdateForm
from books.models import Book


class BookUpdate(UpdateView):
    model = Book
    form_class = BookUpdateForm
    template_name = 'books/edit.html'

    def get_success_url(self):
        return reverse(
            'books:detail',
            kwargs={'book_id': self.object.id}
        )


class BookDetailView(DetailView):

    def get(self, request, *args, **kwargs):
        book = get_object_or_404(Book, pk=kwargs['book_id'])
        context = {'book': book}
        return render(request, 'books/detail.html', context)
