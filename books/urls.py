from django.urls import path

from books import views
app_name = 'books'
urlpatterns = [
    path('book/<int:pk>/edit/', views.BookUpdate.as_view(), name='edit'),
    path('book/<int:book_id>/', views.BookDetailView.as_view(), name='detail'),
]
