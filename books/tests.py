from datetime import date

from django.test import TestCase, Client
from django.urls import reverse

from books.models import Book


class UserTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.book1 = Book.objects.create(
            title="American Tragedy",
            author="Theodore Draiser",
            publication_date=date(1925, 5, 16)
        )
        self.book2 = Book.objects.create(
            title="Titan",
            author="Theodore Draiser",
            publication_date=date(1913, 8, 16)
        )

    def test_book_edit_view(self):
        url = reverse('books:edit', kwargs={'pk': self.book1.id})
        response = self.client.post(url, {
            'title': 'American Tragedy version 2',
            'author': 'Theodore Draiser',
            'publication_date': '2011-08-21'})
        self.assertEqual(response.status_code, 302)
        self.assertIsNotNone(response.content)
        book = Book.objects.get(id=self.book1.id)
        self.assertEqual(book.title, 'American Tragedy version 2')

    def test_detail_view(self):
        url = reverse('books:detail', kwargs={'book_id': self.book1.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
